extends Node2D

# warning-ignore:unused_signal
signal item_grabbed
var picked_up:= false

# Called when the node enters the scene tree for the first time.
func _ready():
	var overlay = load("res://scenes/debug_overlay.tscn").instance()
	overlay.add_stats("Inventory", InventoryController, "inventory", false)
	overlay.add_stats("Ground", InventoryController, "ground_inventory", false)
	add_child(overlay)
