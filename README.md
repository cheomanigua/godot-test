# Godot Inventory testing

- **Cursor keys** to move
- **Space bar/Return** to grab items
- **PageDown** to open inventory

## Expected behavior
When grabbing items from the ground, they will be appended to the inventory

## Actual behavior
- In file `item_instance.gd`, when uncommenting **line 37** and commenting **line 38**, it works as expected.
- In file `item_instance.gd`, when commenting **line 37** and uncommenting **line 38**, it does not works as expected.

## Debugging
- There is a HUD that is constantly showing the inventory content and the ground content
- When grabbing items, a message is attached to the player with the content of inventory, and it is used to check both results when commenting and uncommenting lines 37/38 of `item_instance.gd` 

## Testing playground
- You can initally add or remove items from the inventory by editing the `_ready()` function in file `inventory_controller.gd`
- You can initially place items in the World by instantiating the scene `item_instance`. You can select four types of items and the amount from the inspector.

## What do you expect
- Being able to grab items from the ground and being appended correctly in the inventory.

## Wish list
- Live update of the inventory when opened. At the moment the inventory updates when closing and opening the inventory.
