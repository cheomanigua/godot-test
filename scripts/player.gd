extends KinematicBody2D

export (int) var speed = 70

var velocity = Vector2()

func get_input():
	velocity = Vector2()
	if Input.is_action_pressed('ui_right'):
		velocity.x += 1
	if Input.is_action_pressed('ui_left'):
		velocity.x -= 1
	if Input.is_action_pressed('ui_down'):
		velocity.y += 1
	if Input.is_action_pressed('ui_up'):
		velocity.y -= 1
	velocity = velocity.normalized() * speed

func _physics_process(_delta):
	get_input()
	velocity = move_and_slide(velocity)


func _unhandled_input(event):
	if event.is_action_pressed("ui_accept"):
		grab_item()
	if (event.is_action_pressed("ui_cancel")):
		get_tree().quit()


func grab_item():
	if $PickupZone.items_in_range.size() > 0:
		var pickup_item = $PickupZone.items_in_range.values()
		
		# Be sure to set the $PickupZone's Collision Mask to point to ItemDrop
		for i in pickup_item:
			print(i.name)
			i.pick_up_item(self)
			$PickupZone.items_in_range.erase(i)
			Global.emit_signal("item_grabbed")

func message(message):
	$Label.text = message
