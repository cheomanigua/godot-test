extends Node

var item_data: Dictionary

func open_items_file() -> Dictionary:
	var item_data_file = File.new()
	item_data_file.open("res://data/items3.json", File.READ)
	var item_data_json = JSON.parse(item_data_file.get_as_text())
	item_data_file.close()
	return item_data_json.result

func _ready():
	item_data = open_items_file()
