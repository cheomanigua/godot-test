extends Panel

var ItemClass = preload("res://scenes/item.tscn")
var item = null


func initialize_item(item_name, item_quantity):
	if item == null:
		item = ItemClass.instance()
		add_child(item)
		item.set_item(item_name, item_quantity)
	else:
		item.set_item(item_name, item_quantity)


#func remove_from_slot():
#	remove_child(item)
#	item.free()
#	item = null
