## THIS SCRIPT IS USED TO PICK UP ITEMS FROM THE GROUND
## AND TO ADD THEM TO THE LOGICAL INVENTORY (CONTROLLER)

extends Node

const SlotClass = preload("res://scripts/slot.gd")
const ItemClass = preload("res://scripts/item.gd")
const NUM_INVENTORY_SLOTS = 20
const NUM_GROUND_INVENTORY_SLOTS = 4

var inventory:Dictionary
var ground_inventory:Dictionary


func _ready():
	# Available items in case of testing: Gold, Silver, Gem7 and Gem8
	add_item("Silver", 4)
	add_item("Gem8", 4)

func add_item(item_name, item_quantity):
	for item in inventory:
		if inventory[item][0] == item_name:
			print("%s: %d" % [ground_inventory[item][0], ground_inventory[item][1]])
			var stack_size = int(Data.item_data[item_name]["stack_size"])
			var able_to_add = stack_size - inventory[item][1]
			if able_to_add >= item_quantity:
				inventory[item][1] += item_quantity

				return
			else:
				inventory[item][1] += able_to_add
				item_quantity = item_quantity - able_to_add
	# item doesn't exist in inventory yet, so add it to an empty slot
	for i in range(NUM_INVENTORY_SLOTS):
		if inventory.has(i) == false:
			inventory[i] = [item_name, item_quantity]
			return
#
#
#func remove_item(slot: SlotClass):
## warning-ignore:return_value_discarded
#	inventory.erase(slot.slot_index)
#
#func add_item_to_empty_slot(item: ItemClass, slot: SlotClass):
#	inventory[slot.slot_index] = [item.item_name, item.item_quantity]
#
#func add_item_quantity(slot: SlotClass, quantity_to_add: int):
#	inventory[slot.slot_index][1] += quantity_to_add




######## GROUND #########

func ground_add_item(item_name, item_quantity):
	for item in ground_inventory:
		if ground_inventory[item][0] == item_name:
			var stack_size = int(Data.item_data[item_name]["stack_size"])
			var able_to_add = stack_size - ground_inventory[item][1]
			if able_to_add >= item_quantity:
				ground_inventory[item][1] += item_quantity
				return
			else:
				ground_inventory[item][1] += able_to_add
				item_quantity = item_quantity - able_to_add
	# item doesn't exist in ground_inventory yet, so add it to an empty slot
	for i in range(NUM_GROUND_INVENTORY_SLOTS):
		if ground_inventory.has(i) == false:
			ground_inventory[i] = [item_name, item_quantity]
			return


#func ground_remove_item(slot: SlotClass):
## warning-ignore:return_value_discarded
#	ground_inventory.erase(slot.ground_slot_index)
#
#func ground_add_item_to_empty_slot(item: ItemClass, slot: SlotClass):
#	ground_inventory[slot.ground_slot_index] = [item.item_name, item.item_quantity]
#
#func ground_add_item_quantity(slot: SlotClass, quantity_to_add: int):
#	ground_inventory[slot.ground_slot_index][1] += quantity_to_add
