extends KinematicBody2D

export(String,"Gold","Silver","Gem7","Gem8") var item_name = "Gold"
export (int) var amount = 1

var player = null
var being_picked_up = false


func _ready():
	add_to_group("items")
	$Sprite.texture = load("res://images/" + Data.item_data[item_name]["item_image"])
	pickup(item_name)
# warning-ignore:return_value_discarded
	Global.connect("item_grabbed",self,"_on_item_grabbed")


func pickup(item):
# warning-ignore:return_value_discarded
	$Area2D.connect("body_entered",self,"_on_body_entered",[item])
# warning-ignore:return_value_discarded
	$Area2D.connect("body_exited",self,"_on_body_exited",[item])


func _on_body_entered(body,_item):
	if body.get_name() == "Player":
		InventoryController.ground_add_item(item_name, amount)


func _on_body_exited(body,_item):
	if body.get_name() == "Player":
		InventoryController.ground_inventory.clear()


func _on_item_grabbed():
	if is_queued_for_deletion():
		return
	queue_free()
	var inventory = InventoryController.inventory
	var ground_inventory = InventoryController.ground_inventory
	if being_picked_up == true:
		var offset = inventory.size()
		for i in ground_inventory.size():
			inventory[i + offset] = ground_inventory[i]

func pick_up_item(body):
	player = body
	being_picked_up = true



