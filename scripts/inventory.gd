## SCRIPT USED TO INITIALIZE THE SLOTS OF THE GRAPHICAL INVENTORY (VIEW)
## AND TO TOGGLE GRAPHICAL INVENTORY

extends Node2D

onready var slots = $GridContainer.get_children()

func initialize_inventory():

	for i in range(slots.size()):
		if InventoryController.inventory.has(i):
			slots[i].initialize_item(InventoryController.inventory[i][0], InventoryController.inventory[i][1])


func _unhandled_input(event):
	if event.is_action_pressed("ui_page_down"):
		visible = !visible
		initialize_inventory()
